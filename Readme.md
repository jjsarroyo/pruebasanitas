Instalación
===============

1.- Descargamos el código del repositorio del repositorio

git clone https://gitlab.com/jjsarroyo/sanitas.git

2.- Tenemos que instalar la librería externa tracer.jar , para simplificar la he incluido en la 
carpeta tracer-lib del proyecto. La instalamos ejecutando el siguiente comando:

mvn install:install-file \
   -Dfile=<path_to_library>/tracer-lib/tracer-1.0.0.jar \
   -DgroupId=io.corp.calculator \
   -DartifactId=tracer \
   -Dversion=1.0.0 \
   -Dpackaging=jar \
   -DgeneratePom=true
   
3 .- Arrancamos la aplicación utilizando

mvn spring-boot:run

4 .- Ejecutamos los postman incluidos dentro del proyecto para probar el api. 

Adicionalmente los test se pueden ejecutar con:
 
mvn verify
