package com.sanitas.services.ifaces;

import com.sanitas.model.CalculatorInputRequest;

public interface ICalcService {

  double executeOperation(CalculatorInputRequest inputRequest);
}
