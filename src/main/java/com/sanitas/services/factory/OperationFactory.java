package com.sanitas.services.factory;

import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.model.OperationType;
import com.sanitas.services.factory.ifaces.IOperation;
import com.sanitas.services.factory.impl.SumOperation;
import com.sanitas.services.factory.impl.SustrandOperation;
import org.springframework.stereotype.Component;

@Component
public class OperationFactory {

  public IOperation getOperation(CalculatorInputRequest inputRequest){

    if(inputRequest.getType() == OperationType.SUM){
      return new SumOperation();

    } else return new SustrandOperation();
  }
}
