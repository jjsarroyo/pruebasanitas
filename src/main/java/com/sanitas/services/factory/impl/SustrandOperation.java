package com.sanitas.services.factory.impl;

import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.services.factory.ifaces.IOperation;

public class SustrandOperation implements IOperation {

  @Override
  public double execute(CalculatorInputRequest inputRequest) {

    double result = inputRequest.getOperator1() - inputRequest.getOperator2();
    trace.trace(result);
    return result;
  }
}
