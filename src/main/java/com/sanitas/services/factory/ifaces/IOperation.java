package com.sanitas.services.factory.ifaces;

import com.sanitas.model.CalculatorInputRequest;
import io.corp.calculator.TracerImpl;

public interface IOperation {

  TracerImpl trace = new TracerImpl();

  double execute(CalculatorInputRequest inputRequest);
}
