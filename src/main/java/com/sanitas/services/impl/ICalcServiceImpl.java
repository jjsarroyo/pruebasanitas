package com.sanitas.services.impl;

import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.services.factory.OperationFactory;
import com.sanitas.services.ifaces.ICalcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ICalcServiceImpl implements ICalcService {

  private OperationFactory operationFactory;

  @Autowired
  public ICalcServiceImpl(OperationFactory operationFactory) {
    this.operationFactory = operationFactory;
  }

  @Override
  public double executeOperation(CalculatorInputRequest inputRequest) {
    return operationFactory.getOperation(inputRequest).execute(inputRequest);
  }
}
