package com.sanitas.controller;

import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.model.CalculatorOutput;
import com.sanitas.services.ifaces.ICalcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator/v1/")
public class CalculatorController {

  private final ICalcService service;

  @Autowired
  public CalculatorController(ICalcService service) {
    this.service = service;
  }

  @PostMapping(value = "execute",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public CalculatorOutput executeOperation(@RequestBody CalculatorInputRequest request) {

    return new CalculatorOutput(service.executeOperation(request), HttpStatus.OK);
  }
}
