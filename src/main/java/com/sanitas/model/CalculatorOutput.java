package com.sanitas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Data
public class CalculatorOutput {

  private double result;
  private HttpStatus httpStatus;
}
